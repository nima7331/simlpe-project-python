import time


def countdown(t):
    while t:
        mins, sec = divmod(t, 60)
        timer = '{:02d}:{:02d}'.format(mins, sec)
        print(timer,end='')
        time.sleep(1)
        t -= 1

if __name__ == '__main__':
    user_input = int(input('enter your time :'))
    countdown(user_input)
