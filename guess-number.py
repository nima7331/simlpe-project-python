import random

attempts_list = list()


def show_score():
    if len(attempts_list) <= 0:
        print('There is currently no high score, its yours for taking')
    else:
        print('The current high score is {}'
              ' attempts'.format(min(attempts_list))
              )


def start_game():
    random_number = int(random.randint(1, 10))
    print('Hello traveler ! Wellcome to the game of guess!')
    player_name = input('what,s your name?')
    wanna_play = input(f'Hi {player_name} . would you like to play'
                       f' the guessing game? (Enter Yes/No)')
    attempts = 0
    show_score()
    while wanna_play.lower() == 'yes':
        try:
            guess = input('pick a number between 1 and 10 ')
            if int(guess) < 1 or int(guess) > 10:
                raise ValueError('Please guess a number'
                                 ' within the given range')
            if int(guess) == random_number:
                print('Nice! You got it')
                attempts += 1
                attempts_list.append(attempts)
                print(f'It took you {attempts} attempts')
                play_again = input('Would you like to play'
                                   ' again?(Enter Yes/No)')
                attempts = 0
                show_score()
                random_number = int(random.randint(1, 10))
                if play_again.lower() == 'no':
                    print("That's cool , have a good one!")
                    break
                elif int(guess) < random_number:
                    print('It s higher ')
                    attempts += 1
        except ValueError as err:
            print('Oh no! that is not a valid value. Try again...')
            print(f'({err})')
        else:
            print("That's cool, have a god one!")


if __name__ == '__main__':
    start_game()
