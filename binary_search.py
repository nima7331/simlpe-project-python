import random


def data_creator():
    random_list = list()
    for i in range(100):
        random_list.append(random.randint(1, 100))
    return list(set(sorted(random_list)))


def binary_search(user_input):

    data = data_creator()
    minimum = 0
    maximum = len(data) - 1

    while minimum <= maximum:
        middle = (maximum + minimum) // 2
        if user_input == data[middle]:
            print(f'index {user_input} is {middle}')
            break
        elif user_input > data[middle]:
            minimum = middle + 1
        else:
            maximum = middle - 1
    return -1




if __name__ == '__main__':
    user_input = int(input('enter your number : '))
    print(data_creator())
    binary_search(user_input)
